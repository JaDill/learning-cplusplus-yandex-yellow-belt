#include <map>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <deque>

class Animal
{
public:
    Animal(const std::string& s) : Name(s) {}

    const std::string Name;
};


class Dog : public Animal
{
public:
    Dog(const std::string& s) : Animal(s) {}

    void Bark()
    {
        std::cout << Name << " barks: woof!\n";
    }
};
