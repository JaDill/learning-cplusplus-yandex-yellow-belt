#include <iostream>
#include <string>
#include <vector>

class Man
{
public:
    Man(const std::string& name, const std::string& type = "Man") : _name(name), _type(type) {}

    virtual void walk(std::string destination) const = 0;

    std::string getName() const
    {
        return _name;
    }

    std::string getType() const
    {
        return _type;
    }

    std::string printName() const
    {
        return getType() + ": " + getName();
    }

protected:
    std::string _name;
    std::string _type;
};

class Student : public Man
{
public:

    Student(std::string name, std::string favouriteSong, const std::string& type = "Student") : Man(name, type),
                                                                                                _favouriteSong(
                                                                                                        favouriteSong) {}

    void learn() const
    {
        std::cout << printName() << " learns\n";
    }

    void walk(std::string destination) const override
    {
        std::cout << printName() << " walks to: " << destination << "\n";
        singSong();
    }

    void singSong() const
    {
        std::cout << printName() << " sings a song: " << _favouriteSong << "\n";
    }

private:
    std::string _favouriteSong;
};

class Teacher : public Man
{
public:

    Teacher(std::string name, std::string subject, const std::string& type = "Teacher") : Man(name, type),
                                                                                          _subject(subject) {}

    void teach() const
    {
        std::cout << printName() << " teaches: " << _subject << "\n";
    }

    void walk(std::string destination) const override
    {
        std::cout << printName() << " walks to: " << destination << "\n";
    }

private:
    std::string _subject;
};

class Policeman : public Man
{
public:
    Policeman(std::string name, const std::string& type = "Policeman") : Man(name, type) {}

    void check(const Man& man)
    {
        std::cout << printName() << " checks " << man.getType() << ". " << man.getType() <<
                  "'s name is: " << man.getName() << "\n";
    }

    void walk(std::string destination) const override
    {
        std::cout << printName() << " walks to: " << destination << "\n";
    }
};

void VisitPlaces(const Man& man, const std::vector<std::string>& places)
{
    for (auto p : places)
    {
        man.walk(p);
    }
}

int main()
{
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}