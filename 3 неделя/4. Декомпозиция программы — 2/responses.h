#pragma once
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <ostream>

struct BusesForStopResponse
{
    std::vector<std::string> stops_to_buses;
};

std::ostream& operator<<(std::ostream& out, const BusesForStopResponse& r);

struct StopsForBusResponse
{
    std::vector<std::string> buses_to_stops;
    std::string bus;
    std::map<std::string, std::vector<std::string>> stops_to_buses;
};

std::ostream& operator<<(std::ostream& out, const StopsForBusResponse& r);

struct AllBusesResponse
{
    std::map<std::string, std::vector<std::string>> buses_to_stops;
};

std::ostream& operator<<(std::ostream& out, const AllBusesResponse& r);