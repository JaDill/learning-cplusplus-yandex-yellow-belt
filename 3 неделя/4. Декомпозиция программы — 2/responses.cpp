#include "responses.h"

std::ostream& operator<<(std::ostream& out, const BusesForStopResponse& r)
{
    if (r.stops_to_buses.size() == 0)
    {
        out << "No stop";
    } else
    {
        for (const std::string& bus : r.stops_to_buses)
        {
            out << bus << " ";
        }
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, const StopsForBusResponse& r)
{
    if (r.buses_to_stops.size() == 0)
    {
        out << "No bus";
    } else
    {
        for (const std::string& stop : r.buses_to_stops)
        {
            out << "Stop " << stop << ": ";
            if (r.stops_to_buses.at(stop).size() == 1)
            {
                out << "no interchange";
            } else
            {
                for (const std::string& other_bus : r.stops_to_buses.at(stop))
                {
                    if (r.bus != other_bus)
                    {
                        out << other_bus << " ";
                    }
                }
            }
            if (stop != *r.buses_to_stops.rbegin())
                out << "\n";
        }
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, const AllBusesResponse& r)
{
    if (r.buses_to_stops.empty())
    {
        out << "No buses";
    } else
    {
        for (auto it = r.buses_to_stops.begin(); it != r.buses_to_stops.end(); ++it)
        {
            out << "Bus " << it->first << ": ";
            for (const std::string& stop : it->second)
            {
                out << stop << " ";
            }
            if (++it != r.buses_to_stops.end())
                out << "\n";
            --it;
        }
    }
    return out;
}