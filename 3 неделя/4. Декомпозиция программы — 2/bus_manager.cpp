#include "bus_manager.h"

void BusManager::AddBus(const std::string& bus, const std::vector<std::string>& stops)
{
    _buses_to_stops[bus].insert(_buses_to_stops[bus].begin() + _buses_to_stops[bus].size(), stops.begin(),
                                stops.end());
    for (const std::string& stop : stops)
    {
        _stops_to_buses[stop].push_back(bus);
    }
}

BusesForStopResponse BusManager::GetBusesForStop(const std::string& stop) const
{

    return BusesForStopResponse{_stops_to_buses.find(stop) != _stops_to_buses.end() ? _stops_to_buses.at(stop)
                                                                : std::vector<std::string>{}};
}

StopsForBusResponse BusManager::GetStopsForBus(const std::string& bus) const
{
    return StopsForBusResponse{(this->_buses_to_stops.find(bus) != this->_buses_to_stops.end() ? this->_buses_to_stops.at(bus)
                                                                           : std::vector<std::string>()), bus,
            this->_stops_to_buses};
}

AllBusesResponse BusManager::GetAllBuses() const
{
    return AllBusesResponse{_buses_to_stops};
}