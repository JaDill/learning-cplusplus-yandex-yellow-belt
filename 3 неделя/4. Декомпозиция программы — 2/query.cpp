#include "query.h"

std::istream& operator>>(std::istream& in, Query& q)
{
    q.bus = "";
    q.stop = "";
    q.stops.resize(0);
    std::string command;
    in >> command;
    if (command == "NEW_BUS")
    {
        q.type = QueryType::NewBus;
        in >> q.bus;
        int n;
        in >> n;
        for (int i = 0; i < n; ++i)
        {
            std::string name;
            in >> name;
            q.stops.push_back(name);

        }
    } else if (command == "BUSES_FOR_STOP")
    {
        q.type = QueryType::BusesForStop;
        in >> q.stop;
    } else if (command == "STOPS_FOR_BUS")
    {
        q.type = QueryType::StopsForBus;
        in >> q.bus;
    } else
    {
        q.type = QueryType::AllBuses;
    }

    return in;
}