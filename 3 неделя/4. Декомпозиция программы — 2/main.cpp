#include "query.h"
#include "bus_manager.h"
#include <cassert>

int main()
{
    int query_count;
    Query q;

    std::cin >> query_count;

    BusManager bm;
    for (int i = 0; i < query_count; ++i)
    {
        std::cin >> q;
        switch (q.type)
        {
            case QueryType::NewBus:
                bm.AddBus(q.bus, q.stops);
                break;
            case QueryType::BusesForStop:
                std::cout << bm.GetBusesForStop(q.stop) << "\n";
                break;
            case QueryType::StopsForBus:
                std::cout << bm.GetStopsForBus(q.bus) << "\n";
                break;
            case QueryType::AllBuses:
                std::cout << bm.GetAllBuses() << "\n";
                break;
        }
    }

    return 0;
}