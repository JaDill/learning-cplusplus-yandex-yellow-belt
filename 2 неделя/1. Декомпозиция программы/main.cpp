#include <string>
#include <iostream>
#include <cassert>
#include <vector>
#include <map>

using namespace std;

enum class QueryType
{
    NewBus,
    BusesForStop,
    StopsForBus,
    AllBuses
};

struct Query
{
    QueryType type;
    string bus;
    string stop;
    vector<string> stops;
};

istream& operator>>(istream& in, Query& q)
{
    q.bus = "";
    q.stop = "";
    q.stops.resize(0);
    string command;
    in >> command;
    if (command == "NEW_BUS")
    {
        q.type = QueryType::NewBus;
        in >> q.bus;
        int n;
        in >> n;
        for (int i = 0; i < n; ++i)
        {
            string name;
            in >> name;
            q.stops.push_back(name);

        }
    } else if (command == "BUSES_FOR_STOP")
    {
        q.type = QueryType::BusesForStop;
        in >> q.stop;
    } else if (command == "STOPS_FOR_BUS")
    {
        q.type = QueryType::StopsForBus;
        in >> q.bus;
    } else
    {
        q.type = QueryType::AllBuses;
    }

    return in;
}

struct BusesForStopResponse
{
    vector<string> stops_to_buses;
};

ostream& operator<<(ostream& out, const BusesForStopResponse& r)
{
    if (r.stops_to_buses.size() == 0)
    {
        out << "No stop";
    } else
    {
        for (const string& bus : r.stops_to_buses)
        {
            out << bus << " ";
        }
    }
    return out;
}

struct StopsForBusResponse
{
    map<string, vector<string>> stops_to_buses;
    vector<string> buses_to_stops;
    string bus;
};

ostream& operator<<(ostream& out, const StopsForBusResponse& r)
{
    if (r.buses_to_stops.size() == 0)
    {
        out << "No bus";
    } else
    {
        for (const string& stop : r.buses_to_stops)
        {
            out << "Stop " << stop << ": ";
            if (r.stops_to_buses.at(stop).size() == 1)
            {
                out << "no interchange";
            } else
            {
                for (const string& other_bus : r.stops_to_buses.at(stop))
                {
                    if (r.bus != other_bus)
                    {
                        out << other_bus << " ";
                    }
                }
            }
            if (stop != *r.buses_to_stops.rbegin())
                out << "\n";
        }
    }
    return out;
}

struct AllBusesResponse
{
    map<string, vector<string>> buses_to_stops;
};

ostream& operator<<(ostream& out, const AllBusesResponse& r)
{
    if (r.buses_to_stops.empty())
    {
        out << "No buses";
    } else
    {
        for (auto it = r.buses_to_stops.begin(); it != r.buses_to_stops.end(); ++it)
        {
            out << "Bus " << it->first << ": ";
            for (const string& stop : it->second)
            {
                out << stop << " ";
            }
            if (++it != r.buses_to_stops.end())
                out << "\n";
            --it;
        }
    }
    return out;
}

class BusManager
{
public:
    void AddBus(const string& bus, const vector<string>& stops)
    {
        _buses_to_stops[bus].insert(_buses_to_stops[bus].begin() + _buses_to_stops[bus].size(), stops.begin(),
                                    stops.end());
        for (const string& stop : stops)
        {
            _stops_to_buses[stop].push_back(bus);
        }
    }

    BusesForStopResponse GetBusesForStop(const string& stop) const
    {
        return {_stops_to_buses.find(stop) != _stops_to_buses.end() ? _stops_to_buses.at(stop) : vector<string>{}};
    }

    StopsForBusResponse GetStopsForBus(const string& bus) const
    {
        return {_stops_to_buses,
                _buses_to_stops.find(bus) != _buses_to_stops.end() ? _buses_to_stops.at(bus) : vector<string>{}, bus};
    }

    AllBusesResponse GetAllBuses() const
    {
        return {_buses_to_stops};
    }

private:
    map<string, vector<string>> _buses_to_stops, _stops_to_buses;

};

// Не меняя тела функции main, реализуйте функции и классы выше

int main()
{
    int query_count;
    Query q;

    cin >> query_count;

    BusManager bm;
    for (int i = 0; i < query_count; ++i)
    {
        cin >> q;
        switch (q.type)
        {
            case QueryType::NewBus:
                bm.AddBus(q.bus, q.stops);
                break;
            case QueryType::BusesForStop:
                cout << bm.GetBusesForStop(q.stop) << endl;
                break;
            case QueryType::StopsForBus:
                cout << bm.GetStopsForBus(q.bus) << endl;
                break;
            case QueryType::AllBuses:
                cout << bm.GetAllBuses() << endl;
                break;
        }
    }

    return 0;
}