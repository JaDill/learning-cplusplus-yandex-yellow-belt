#include <vector>
#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <iterator>
#include <algorithm>
#include <numeric>

int fact(int N)
{
    if (N < 0) // ���� ������������ ���� ������������� �����
        return 0; // ���������� ����
    if (N == 0) // ���� ������������ ���� ����,
        return 1; // ���������� ��������� �� ���� - �� �����������, �� ��� 1 =)
    else // �� ���� ��������� �������
        return N * fact(N - 1); // ������ ��������.
}

int main()
{
    int n;
    std::cin >> n;
    std::vector<int> a(n);
    std::iota(a.begin(), a.end(), 1);
    std::reverse(a.begin(),a.end());
    int N = fact(n);
    for (int i = 0; i < N; ++i)
    {
        for (const auto& it : a)
            std::cout << it << " ";
        std::cout << "\n";
        std::prev_permutation(a.begin(), a.end());
    }
}