#include <vector>
#include <iostream>
#include <algorithm>


template<typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    if (range_end - range_begin < 2)
        return;
    std::vector<typename RandomIt::value_type> temp(range_begin, range_end);
    auto it = temp.size() / 2 + temp.begin();
    MergeSort(temp.begin(), it);
    MergeSort(it, temp.end());
    std::merge(temp.begin(), it, it, temp.end(), range_begin);
}

int main()
{
    std::vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1};
    MergeSort(begin(v), end(v));
    for (int x : v)
    {
        std::cout << x << " ";
    }
    return 0;
}