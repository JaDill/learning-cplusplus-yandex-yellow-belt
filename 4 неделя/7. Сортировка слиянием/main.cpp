#include <vector>
#include <iostream>
#include <algorithm>


template<typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    if (range_end - range_begin < 2)
        return;
    std::vector<typename RandomIt::value_type> temp(range_begin, range_end);

    auto it1 = temp.size() / 3 + temp.begin(), it2 = 2 * temp.size() / 3 + temp.begin();
    MergeSort(temp.begin(), it1);
    MergeSort(it1, it2);
    MergeSort(it2, temp.end());
    std::vector<typename RandomIt::value_type> temp2;
    std::merge(temp.begin(), it1, it1, it2, std::back_inserter(temp2));
    std::merge(temp2.begin(), temp2.end(), it2, temp.end(), range_begin);
}

int main() {
    std::vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1, 5};
    MergeSort(begin(v), end(v));
    for (int x : v) {
        std::cout << x << " ";
    }
    return 0;
}