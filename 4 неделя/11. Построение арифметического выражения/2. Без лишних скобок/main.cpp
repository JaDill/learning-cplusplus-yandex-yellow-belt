#include <map>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <deque>


int main()
{
    int number, n;
    std::cin >> number >> n;
    std::deque<std::string> v = {std::to_string(number)};
    char sign = ' ', prefSign;
    for (int i = 0; i < n; ++i)
    {
        prefSign = sign;
        std::cin >> sign >> number;
        if ((sign == '*' || sign == '/') && (prefSign == '+' || prefSign == '-'))
        {
            v.push_front("(");
            v.emplace_back(")");
        }
        v.push_back(" " + std::string(1, sign) + " " + std::to_string(number));
    }
    for (const auto& item : v)
        std::cout << item;
    return 0;
}