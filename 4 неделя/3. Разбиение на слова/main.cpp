#include <algorithm>
#include <string>
#include <vector>

using namespace std;

vector<string> SplitIntoWords(const string& str) {
  // ������� ������, � ������� ����� ��������� �����
  vector<string> result;

  // ��� ��� ��������� ����� ������������ ������� �� �������� ����� ������
  // str_begin ����� ��������� ������
  auto str_begin = begin(str);
  // str_end ������ ����� ��������� �� ����� ������ (������� �� �����������)
  const auto str_end = end(str);

  // � ����� ��� ������� ����������, ��� ��� ������������ ������ ��������� ��� �
  // ������� break
  while (true) {
    
    // ������� ������ ������ � ������� ������� ������
    auto it = find(str_begin, str_end, ' ');
    
    // ������������ [str_begin, it) � ��������� �����
    result.push_back(string(str_begin, it));
    
    if (it == str_end) {
      // ���� �������� ������ ���, ���� ���� ���������.
      // ��������� ����� ��� ���������
      break;
    } else {
      // ����� ������ ������ ����� ������� � ������ ���������� �����.
      // ���� � �������� str_begin
      str_begin = it + 1;
    }
    
  }

  return result;
}
