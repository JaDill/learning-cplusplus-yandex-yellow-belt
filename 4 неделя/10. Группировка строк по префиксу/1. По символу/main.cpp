#include <map>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>


template<typename RandomIt>
std::pair<RandomIt, RandomIt> FindStartsWith(RandomIt range_begin, RandomIt range_end, char prefix)
{
    std::string pref(1, prefix);
    std::string pref2(1, ++prefix);
    auto first = std::lower_bound(range_begin, range_end, pref);
    auto second = std::upper_bound(first, range_end, pref2);
    return {first, second};
}


int main()
{
    const std::vector<std::string> sorted_strings = {"moscow", "murmansk", "vologda"};

    const auto m_result =
            FindStartsWith(begin(sorted_strings), end(sorted_strings), 'm');
    for (auto it = m_result.first; it != m_result.second; ++it)
    {
        std::cout << *it << " ";
    }
    std::cout << "\n";

    const auto p_result =
            FindStartsWith(begin(sorted_strings), end(sorted_strings), 'p');
    std::cout << (p_result.first - begin(sorted_strings)) << " " <<
              (p_result.second - begin(sorted_strings)) << "\n";

    const auto z_result =
            FindStartsWith(begin(sorted_strings), end(sorted_strings), 'z');
    std::cout << (z_result.first - begin(sorted_strings)) << " " <<
              (z_result.second - begin(sorted_strings));

    return 0;
}