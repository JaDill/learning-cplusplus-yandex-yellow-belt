#include <map>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>


template<typename RandomIt>
std::pair<RandomIt, RandomIt> FindStartsWith(RandomIt range_begin, RandomIt range_end, const std::string& prefix)
{
    std::string pref2(prefix);
    (*pref2.rbegin())++;
    auto first = std::lower_bound(range_begin, range_end, prefix,
                                  [](const std::string& x, const std::string y)
                                  {
                                      return x < y;
                                  });
    auto second = std::lower_bound(first, range_end, pref2,
                                   [](const std::string& x, const std::string y)
                                   {
                                       return x < y;
                                   });
    return {first, second};
}


int main()
{
    const std::vector<std::string> sorted_strings = {"moscow", "motovilikha", "murmansk"};

    const auto mo_result =
            FindStartsWith(begin(sorted_strings), end(sorted_strings), "mo");
    for (auto it = mo_result.first; it != mo_result.second; ++it)
    {
        std::cout << *it << " ";
    }
    std::cout << "\n";

    const auto mt_result =
            FindStartsWith(begin(sorted_strings), end(sorted_strings), "mt");
    std::cout << (mt_result.first - begin(sorted_strings)) << " " <<
              (mt_result.second - begin(sorted_strings)) << "\n";

    const auto na_result =
            FindStartsWith(begin(sorted_strings), end(sorted_strings), "na");
    std::cout << (na_result.first - begin(sorted_strings)) << " " <<
              (na_result.second - begin(sorted_strings));

    return 0;
}