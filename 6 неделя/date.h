#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

class Date
{
public:
    Date();

    Date(int year, int month, int day);

    Date(std::istream& in);

public:
    friend std::istream& operator>>(std::istream& in, Date& date);

    friend std::ostream& operator<<(std::ostream& out, const Date& date);

    friend bool operator<(const Date& lhv, const Date& rhv);

    friend bool operator>(const Date& lhv, const Date& rhv);

    friend bool operator==(const Date& lhv, const Date& rhv);

    friend bool operator<=(const Date& lhv, const Date& rhv);

    friend bool operator>=(const Date& lhv, const Date& rhv);

    friend bool operator!=(const Date& lhv, const Date& rhv);

public:
    int getYear() const;

    int getMonth() const;

    int getDay() const;

private:
    void makeDate(std::istream& in);

private:
    int _day, _month, _year;
};

Date ParseDate(std::istream& in);