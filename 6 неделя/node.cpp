#include "node.h"

Node::Node(Comparison cmp) : _cmp(cmp) {}

bool EmptyNode::Evaluate(const Date& date, const std::string& event)
{
    return true;
}

DateComparisonNode::DateComparisonNode(Comparison cmp, const Date& date) : Node(cmp), _date(date) {}

bool DateComparisonNode::Evaluate(const Date& date, const std::string& event)
{
    switch (_cmp)
    {
        case Comparison::Equal:
            return date == _date;
        case Comparison::Greater:
            return date > _date;
        case Comparison::GreaterOrEqual:
            return date >= _date;
        case Comparison::Less:
            return date < _date;
        case Comparison::LessOrEqual:
            return date <= _date;
        case Comparison::NotEqual:
            return date != _date;
    }
}

EventComparisonNode::EventComparisonNode(Comparison cmp, const std::string& event) : Node(cmp), _event(event) {}

bool EventComparisonNode::Evaluate(const Date& date, const std::string& event)
{
    switch (_cmp)
    {
        case Comparison::Equal:
            return event == _event;
        case Comparison::Greater:
            return event > _event;
        case Comparison::GreaterOrEqual:
            return event >= _event;
        case Comparison::Less:
            return event < _event;
        case Comparison::LessOrEqual:
            return event <= _event;
        case Comparison::NotEqual:
            return event != _event;
    }
}

LogicalOperationNode::LogicalOperationNode(LogicalOperation logOp, const std::shared_ptr<Node>& left,
                                           const std::shared_ptr<Node>& right) : _logOp(logOp), _left(left),
                                                                                 _right(right) {}

bool LogicalOperationNode::Evaluate(const Date& date, const std::string& event)
{
    switch (_logOp)
    {
        case LogicalOperation::And:
            return _left->Evaluate(date, event) && _right->Evaluate(date, event);
        case LogicalOperation::Or:
            return _left->Evaluate(date, event) || _right->Evaluate(date, event);

    }
}
