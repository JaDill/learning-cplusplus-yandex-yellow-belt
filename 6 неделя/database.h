#pragma once

#include "date.h"
#include <map>
#include <set>
#include <vector>

class Database
{
public:
    void Add(const Date& date, const std::string& event);

    void Print(std::ostream& out) const;

    std::string Last(const Date& date) const;

    template<typename Func>
    int RemoveIf(Func func)
    {
        int count = 0;
        for (auto it = _calendarToVector.begin(); it != _calendarToVector.end(); ++it)
        {
            for (int i = 0; i < it->second.size(); ++i)
                if (func(it->first, it->second[i]))
                {
                    _calendarToSet[it->first].erase(it->second[i]);
                    it->second.erase(it->second.begin() + i);
                    --i;
                    ++count;
                }
        }
        for (auto it = _calendarToVector.begin(); it != _calendarToVector.end();)
            if (it->second.empty())_calendarToVector.erase(it++);
            else it++;

        for (auto it = _calendarToSet.begin(); it != _calendarToSet.end();)
            if (it->second.empty())_calendarToSet.erase(it++);
            else it++;
        return count;

    }

    template<typename Func>
    std::vector<std::string> FindIf(Func func) const
    {
        std::vector<std::string> result;
        for (auto it = _calendarToVector.begin(); it != _calendarToVector.end(); ++it)
            for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2)
                if (func(it->first, *it2))
                {
                    std::stringstream out;
                    out << it->first << " " << *it2;
                    std::string line;
                    std::getline(out, line);
                    result.push_back(line);
                }
        return result;
    }

private:
    std::map<Date, std::set<std::string>> _calendarToSet;
    std::map<Date, std::vector<std::string>> _calendarToVector;
};