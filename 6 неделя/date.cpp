#include "date.h"

Date::Date() : _day(INT32_MAX), _month(INT32_MAX), _year(INT32_MAX) {}

Date::Date(int year, int month, int day) : _day(day), _month(month), _year(year) {}

Date::Date(std::istream& in)
{
    makeDate(in);
}

std::istream& operator>>(std::istream& in, Date& date)
{
    date.makeDate(in);
    return in;
}

std::ostream& operator<<(std::ostream& out, const Date& date)
{
    out << std::setw(4) << std::setfill('0') << date._year << "-" <<
        std::setw(2) << std::setfill('0') << date._month << "-" <<
        std::setw(2) << std::setfill('0') << date._day;
    return out;
}

bool operator<(const Date& lhv, const Date& rhv)
{
    if (lhv._year == rhv._year)
    {
        if (lhv._month == rhv._month)
            return lhv._day < rhv._day;
        return lhv._month < rhv._month;
    }
    return lhv._year < rhv._year;
}

bool operator>(const Date& lhv, const Date& rhv)
{
    if (lhv._year == rhv._year)
    {
        if (lhv._month == rhv._month)
            return lhv._day > rhv._day;
        return lhv._month > rhv._month;
    }
    return lhv._year > rhv._year;
}

bool operator==(const Date& lhv, const Date& rhv)
{
    return lhv._year == rhv._year && lhv._month == rhv._month && lhv._day == rhv._day;
}

bool operator<=(const Date& lhv, const Date& rhv)
{
    return !(lhv > rhv);
}

bool operator>=(const Date& lhv, const Date& rhv)
{
    return !(lhv < rhv);
}

bool operator!=(const Date& lhv, const Date& rhv)
{
    return !(lhv == rhv);
}

int Date::getYear() const
{
    return _year;
}

int Date::getMonth() const
{
    return _month;
}

int Date::getDay() const
{
    return _day;
}

void Date::makeDate(std::istream& in)
{
    std::string date;
    in >> date;
    std::stringstream ss(date);
    bool needThrow = ss.eof();

    char def1, def2;
    ss >> _year;
    needThrow = needThrow ? true : ss.eof();
    ss >> def1;
    needThrow = needThrow ? true : ss.eof();
    ss >> _month;
    needThrow = needThrow ? true : ss.eof();
    ss >> def2;
    needThrow = needThrow ? true : ss.eof();
    ss >> _day;
    needThrow = needThrow ? true : !ss.eof();

    if (needThrow || def1 != '-' || def2 != '-' || _month == INT32_MAX || _day == INT32_MAX)
        throw std::invalid_argument("Wrong date format: " + date);

    if (_month < 1 || _month > 12)
        throw std::invalid_argument("Month value is invalid: " + std::to_string(_month));

    if (_day < 1 || _day > 31)
        throw std::invalid_argument("Day value is invalid: " + std::to_string(_day));
}

Date ParseDate(std::istream& in)
{
    return Date(in);
}