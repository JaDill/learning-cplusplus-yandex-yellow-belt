#pragma once

#include "date.h"
#include "enums.h"
#include <memory>

class Node
{
public:
    Node(){}

    Node(Comparison cmp);

    virtual bool Evaluate(const Date& date, const std::string& event) = 0;

protected:
    Comparison _cmp;
};

class EmptyNode : public Node
{
    bool Evaluate(const Date& date, const std::string& event) override;
};

class DateComparisonNode : public Node
{
public:
    DateComparisonNode(Comparison cmp, const Date& date);

    bool Evaluate(const Date& date, const std::string& event) override ;

private:
    Date _date;
};

class EventComparisonNode : public Node
{
public:
    EventComparisonNode(Comparison cmp, const std::string& event);

    bool Evaluate(const Date& date, const std::string& event);

private:
    std::string _event;
};

class LogicalOperationNode : public Node
{
public:
    LogicalOperationNode(LogicalOperation logOp, const std::shared_ptr<Node>& left, const std::shared_ptr<Node>& right);

    bool Evaluate(const Date& date, const std::string& event) override ;

private:
    LogicalOperation _logOp;
    std::shared_ptr<Node> _left;
    std::shared_ptr<Node> _right;
};