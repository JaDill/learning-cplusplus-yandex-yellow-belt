#include "database.h"
#include <algorithm>
#include <iterator>

void Database::Add(const Date& date, const std::string& event)
{
    auto flag = _calendarToSet[date].insert(event);
    if (flag.second)
    {
        _calendarToVector[date].push_back(event);
    }
}

void Database::Print(std::ostream& out) const
{
    for (const auto& i : _calendarToVector)
        for (const auto& event : i.second)
            out << i.first << " " << event << "\n";
}

std::string Database::Last(const Date& date) const
{
    auto it = _calendarToVector.upper_bound(date);
    if (it == _calendarToVector.begin())
        return "No entries";
    --it;
    std::stringstream out;
    out<< it->first << " " << it->second.back();
    std::string result;
    std::getline(out, result);
    return result;
}