#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <exception>


int main()
{
    int n, r;
    std::cin >> n >> r;
    unsigned long long sum = 0;
    int w, h, d;
    for (int i = 0; i < n; ++i)
    {
        std::cin >> w >> h >> d;
        sum += r * w * h * d;
    }
    std::cout << sum;

    return 0;
}