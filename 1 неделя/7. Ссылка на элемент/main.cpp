#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <exception>
#include <map>
#include <algorithm>
#include <tuple>

using namespace std;

template<typename First, typename Second>
Second& GetRefStrict(map<First, Second>& map, First i)
{
    auto result = map.find(i);
    if(result == map.end())
        throw runtime_error("");
    return result->second;
}

int main()
{
    map<int, string> m = {{0, "value"}};
    string& item = GetRefStrict(m, 0);
    item = "newvalue";
    cout << m[0] << endl; // выведет newvalue

    return 0;
}