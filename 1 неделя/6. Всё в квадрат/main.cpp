#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <exception>
#include <map>
#include <algorithm>
#include <tuple>

using namespace std;

template<typename T>
vector<T> operator*(const vector<T>& lhv, const vector<T>& rhv);

template<typename First, typename Second>
map<First, Second> operator*(const map<First, Second>& lhv, const map<First, Second>& rhv);

template<typename First, typename Second>
pair<First, Second> operator*(const pair<First, Second>& lhv, const pair<First, Second>& rhv)
{
    return {lhv.first * rhv.first, lhv.second * rhv.second};
}

template<typename First, typename Second>
map<First, Second> operator*(const map<First, Second>& lhv, const map<First, Second>& rhv)
{
    map<First, Second> result;
    for (auto itl = lhv.begin(), itr = rhv.begin(); itl != lhv.end() && itr != rhv.end(); ++itl, ++itr)
        result[itl->first] = itl->second * itr->second;
    return result;
}

template<typename T>
vector<T> operator*(const vector<T>& lhv, const vector<T>& rhv)
{
    vector<T> result;
    for (size_t i = 0; i < lhv.size(); ++i)
        result.push_back(lhv[i] * rhv[i]);
    return result;
}

template<typename T>
T Sqr(const T& collection)
{
    T result(collection * collection);
    return result;
}


int main()
{
    vector<int> v = {1, 2, 3};
    vector<int> v1{4, 5, 6};
    map<vector<int>, vector<int>> Map_vector{
            {v,  v1},
            {v1, v}
    };
    for (auto& item : Sqr(Map_vector))
    {
        for (auto& v : item.second)
        {
            cout << v << ' ';
        }
        cout << '\n';
    }

    return 0;
}