#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <exception>

using namespace std;

class Matrix
{
public:
    Matrix()
    {
        _numRows = _numCols = 0;
    }

    Matrix(int numRows, int numCols)
    {
        Reset(numRows, numCols);
    }

public:
    friend std::istream& operator>>(std::istream& in, Matrix& rhv)
    {
        int height, width;
        in >> height >> width;
        rhv.Reset(height, width);
        for (int i = 0; i < height; ++i)
        {
            for (int j = 0; j < width; ++j)
            {
                in >> rhv._matrix[i][j];
            }
        }
        return in;
    }

    friend std::ostream& operator<<(std::ostream& out, const Matrix& rhv)
    {
        int height = rhv.GetNumRows(), width = rhv.GetNumColumns();
        out << height << " " << width << "\n";
        for (int i = 0; i < height; ++i)
        {
            for (int j = 0; j < width; ++j)
            {
                out << rhv._matrix[i][j] << " ";
            }
            out << "\n";
        }
        return out;
    }

    friend bool operator==(const Matrix& lhv, const Matrix& rhv)
    {
        int height = lhv.GetNumRows(), width = lhv.GetNumColumns();
        if (height == rhv.GetNumRows() && width == rhv.GetNumColumns())
        {
            for (int i = 0; i < height; ++i)
                for (int j = 0; j < width; ++j)
                    if (lhv.At(i, j) != rhv.At(i, j))
                        return false;
            return true;
        }
        return false;
    }

    friend Matrix operator+(const Matrix& lhv, const Matrix& rhv)
    {
        int height = lhv.GetNumRows(), width = lhv.GetNumColumns();
        if (height != rhv.GetNumRows() || width != rhv.GetNumColumns())
            throw std::invalid_argument("different size");

        Matrix result(height, width);
        for (int i = 0; i < height; ++i)
            for (int j = 0; j < width; ++j)
                result.At(i, j) = lhv.At(i, j) + rhv.At(i, j);
        return result;
    }

public:
    void Reset(int numRows, int numCols)
    {
        if (numCols < 0 || numRows < 0)
            throw std::out_of_range("negative params");

        if (numRows == 0 || numCols == 0)
            numRows = numCols = 0;

        _numRows = numRows;
        _numCols = numCols;
        _matrix.assign(numRows, std::vector<int>(numCols));
    }

    int At(int indexRows, int indexCols) const
    {
        return _matrix.at(indexRows).at(indexCols);
    }

    int& At(int indexRows, int indexCols)
    {
        return _matrix.at(indexRows).at(indexCols);
    }

    int GetNumRows() const
    {
        return _numRows;
    }

    int GetNumColumns() const
    {
        return _numCols;
    }

private:
    int _numRows;
    int _numCols;
    std::vector<std::vector<int>> _matrix;

};

int main()
{
    Matrix a, b;
    std::cin >> a >> b;
    std::cout << a + b;

    return 0;
}