#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <exception>
#include <map>
#include <algorithm>
#include <tuple>

using namespace std;


class TeamTasks
{
public:
    // Получить статистику по статусам задач конкретного разработчика
    const TasksInfo& GetPersonTasksInfo(const string& person) const
    {
        return team.at(person);
    }

    // Добавить новую задачу (в статусе NEW) для конкретного разработчитка
    void AddNewTask(const string& person)
    {
        team[person][TaskStatus::NEW]++;
    }

    // Обновить статусы по данному количеству задач конкретного разработчика,
    // подробности см. ниже
    tuple<TasksInfo, TasksInfo> PerformPersonTasks(const string& person, int task_count)
    {
        TasksInfo news, old(team[person]);
        int r;
        if (task_count > 0 && old.find(TaskStatus::NEW) != old.end())
        {
            if (old[TaskStatus::NEW] - task_count > 0)
            {
                old[TaskStatus::NEW] -= task_count;
                news[TaskStatus::IN_PROGRESS] += task_count;
                task_count = 0;
            } else
            {
                r = old[TaskStatus::NEW];
                old.erase(TaskStatus::NEW);
                news[TaskStatus::IN_PROGRESS] += r;
                task_count -= r;
            }
        }
        if (task_count > 0 && old.find(TaskStatus::IN_PROGRESS) != old.end())
        {
            if (old[TaskStatus::IN_PROGRESS] - task_count > 0)
            {
                old[TaskStatus::IN_PROGRESS] -= task_count;
                news[TaskStatus::TESTING] += task_count;
                task_count = 0;
            } else
            {
                r = old[TaskStatus::IN_PROGRESS];
                old.erase(TaskStatus::IN_PROGRESS);
                news[TaskStatus::TESTING] += r;
                task_count -= r;
            }
        }
        if (task_count > 0 && old.find(TaskStatus::TESTING) != old.end())
        {
            if (old[TaskStatus::TESTING] - task_count > 0)
            {
                old[TaskStatus::TESTING] -= task_count;
                news[TaskStatus::DONE] += task_count;
            } else
            {
                r = old[TaskStatus::TESTING];
                old.erase(TaskStatus::TESTING);
                news[TaskStatus::DONE] += r;
            }
        }

        TasksInfo news2(news);
        for (const auto& it : old)
            news2[it.first] += it.second;
        team[person] = news2;
        old.erase(TaskStatus::DONE);
        return tie(news, old);
    }

private:
    map<string, TasksInfo> team;
};

// Принимаем словарь по значению, чтобы иметь возможность
// обращаться к отсутствующим ключам с помощью [] и получать 0,
// не меняя при этом исходный словарь
void PrintTasksInfo(TasksInfo tasks_info)
{
    cout << tasks_info[TaskStatus::NEW] << " new tasks" <<
         ", " << tasks_info[TaskStatus::IN_PROGRESS] << " tasks in progress" <<
         ", " << tasks_info[TaskStatus::TESTING] << " tasks are being tested" <<
         ", " << tasks_info[TaskStatus::DONE] << " tasks are done" << endl;
}

int main()
{
    TeamTasks tasks;
    for (int i = 0; i < 5; ++i)
    {
        tasks.AddNewTask("Alice");
    }
    cout << "Alice's tasks: ";
    PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));

    TasksInfo updated_tasks, untouched_tasks;

    tie(updated_tasks, untouched_tasks) =
            tasks.PerformPersonTasks("Alice", 5);
    cout << "Updated Alice's tasks: ";
    PrintTasksInfo(updated_tasks);
    cout << "Untouched Alice's tasks: ";
    PrintTasksInfo(untouched_tasks);

    tie(updated_tasks, untouched_tasks) =
            tasks.PerformPersonTasks("Alice", 5);
    cout << "Updated Alice's tasks: ";
    PrintTasksInfo(updated_tasks);
    cout << "Untouched Alice's tasks: ";
    PrintTasksInfo(untouched_tasks);

    tie(updated_tasks, untouched_tasks) =
            tasks.PerformPersonTasks("Alice", 1);
    cout << "Updated Alice's tasks: ";
    PrintTasksInfo(updated_tasks);
    cout << "Untouched Alice's tasks: ";
    PrintTasksInfo(untouched_tasks);

    for (int i = 0; i < 5; ++i)
    {
        tasks.AddNewTask("Alice");
    }
    PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));

    tie(updated_tasks, untouched_tasks) =
            tasks.PerformPersonTasks("Alice", 2);
    cout << "Updated Alice's tasks: ";
    PrintTasksInfo(updated_tasks);
    cout << "Untouched Alice's tasks: ";
    PrintTasksInfo(untouched_tasks);

    PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));

    tie(updated_tasks, untouched_tasks) =
            tasks.PerformPersonTasks("Alice", 4);
    cout << "Updated Alice's tasks: ";
    PrintTasksInfo(updated_tasks);
    cout << "Untouched Alice's tasks: ";
    PrintTasksInfo(untouched_tasks);

    PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));


    return 0;
}